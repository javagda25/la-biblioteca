package com.javagda25.labiblioteca.model;

public interface IBaseEntity {
    Long getId();
}
