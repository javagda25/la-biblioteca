package com.javagda25.labiblioteca.dao;

import com.javagda25.labiblioteca.model.Book;
import com.javagda25.labiblioteca.model.BookLent;
import com.javagda25.labiblioteca.model.Client;
import com.javagda25.labiblioteca.util.HibernateUtil;
import org.hibernate.Session;

import javax.persistence.criteria.*;
import java.util.List;

public class BookLentDao {
    public void getBooksLentByClient(Long clientId) {
        // select * from BookLent bl join Book b on bl.book_id = b.id where bl.client_id = ?
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();

            CriteriaQuery<Book> criteriaQuery = cb.createQuery(Book.class);

            Root<BookLent> blRoot = criteriaQuery.from(BookLent.class);
            Join<BookLent, Book> joinBook = blRoot.join("book", JoinType.LEFT);
            Join<BookLent, Book> joinClient = blRoot.join("client", JoinType.LEFT);

            criteriaQuery.select(cb.construct(Book.class,
                    joinBook.get("id"),
                    joinBook.get("title"),
                    joinBook.get("yearWritten"),
                    joinBook.get("numberOfPages"),
                    joinBook.get("numberOfAvailableCopies")
            )).where(
                    cb.equal(joinClient.get("id"), clientId)
            );

            session.createQuery(criteriaQuery).list().forEach(System.out::println);
        }
    }
}
